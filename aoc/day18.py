with open('day18.txt') as f:
    all_cubes = f.read().strip().splitlines()
seq_cubes=[]
for i in all_cubes:
    lst = i.split(",")
    x, y, z = 0, 0, 0
    for i, val in enumerate(lst):
        if i == 0:
            x = int(val)
        elif i == 1:
            y = int(val)
        elif i == 2:
            z = int(val)
    #make list upto end in iterations by adding one x,y,x values
    seq_cubes.append((x,y,z))
# dictionary to check 
filter= {
    "right": (1, 0, 0),
    "left": (-1, 0, 0),
    "up": (0, 1, 0),
    "down": (0, -1, 0),
    "front": (0, 0, 1),
    "back": (0, 0, -1)
}
count=0
# increase counter by 6 after assigning coordinate values
i = 0
while i < len(seq_cubes):
    x_coor, y_coor, z_coor = seq_cubes[i]
    count += 6
    for key, offset in filter.items():
        dx_coor, dy_coor, dz_coor = offset
        # add offset of coordinates to the each coordinate 
        add_offset = x_coor + dx_coor, y_coor + dy_coor, z_coor + dz_coor
        # compare add_offset to the original sequence
        if (add_offset) not in seq_cubes:
            continue
        else:
            count -= 1
    i += 1

#Answer-1
print(count)


###Part-02###
new_line = list([(0,0,0)])
hit = []
count_1=0
for i, all_coor in enumerate(new_line):
    x_coor_, y_coor_, z_coor_=all_coor
    #for each iteration add the values to hit
    hit.extend([(x_coor_, y_coor_, z_coor_)])
    for key, offset in filter.items():
        dx_coor, dy_coor, dz_coor = offset
        # add offset of coordinates to the each coordinate 
        added_offset = (x_coor_ + dx_coor, y_coor_ + dy_coor, z_coor_ + dz_coor)
        #if we have coordinates from -1 to 22, flag will true, otherwise breaks the loop
        flag = True
        for coord in added_offset:
            if coord < -1 or coord > 22:
                flag = False
                break
        #if flag is true and added offset not in hit and not in new_line
        if flag and added_offset not in hit and added_offset not in new_line:
        # check if added offset is in original sequence, then increment counter by 1    
            if added_offset in seq_cubes:
                count_1 += 1
            else:
        # if added offset is not in original sequence, add the added_offset to newline and loop iterates again
                new_line.extend([added_offset])
#Answer-2
print(count_1)

##### Reference #####
# (https://www.reddit.com/r/adventofcode/comments/zoqhvy/2022_day_18_solutions/)
