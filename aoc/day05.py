with open("day05.txt", "rt") as myfile:
    my_list = myfile.read()
    my_list=my_list.split('\n\n')
#getting stacks
my_newlist=my_list[0]
#separate numbers from stacks
split_newlist=my_newlist.splitlines()
# numbers like 1 2 3 4 5 6 
numbers_to_stack=split_newlist[-1]
# split numbers 
space_splitting=numbers_to_stack.split()
#divide by length of column and placed all in lists of list
len_list=len(space_splitting)
lists_of_list = []
for i in range(len_list):
    lists_of_list.append([])
# iterate on stacks excluding numbers
for stacks in split_newlist[:-1]:
    for stack in range(1,len(stacks),4):
        # if these are not space string and are alphabets
        if stacks[stack]!=' ':
            ind_c=stack//4
            # place alphabets at that indices in lists
            lists_of_list[ind_c].append(stacks[stack])
# reverse all the lists to perform operations
lists_of_list = [x[::-1] for x in lists_of_list]
#coming to movement portion
move_list=my_list[1]
a=move_list.splitlines()
for array in a:
    b=array.split(' ')
    if len(b)==6:
       numbers,fr,to=int(b[1]), int(b[3]), int(b[5])
       f=lists_of_list[fr-1]
       # pops and appends the alphabets using lists of list
       for _ in range(numbers):        
        lists_of_list[to-1].append(f.pop())
# Appending all last elements in a new final list
ordered_ones = [lists[-1] for lists in lists_of_list]
#Answer-1
print(ordered_ones)


###Part-02###
with open("day05.txt", "rt") as myfile:
    my_list = myfile.read()
    my_list=my_list.split('\n\n')
#getting stacks
my_newlist=my_list[0]
#separate numbers from stacks
split_newlist=my_newlist.splitlines()
numbers_to_stack=split_newlist[-1]
space_splitting=numbers_to_stack.split()
#divide by length of column and placed all in lists of list
len_list=len(space_splitting)
lists_of_list=[[] for i in range(len_list)]
for stacks in split_newlist[:-1]:
    for stack in range(1,len(stacks),4):
        if stacks[stack]!=' ':
            ind_c=stack//4
            lists_of_list[ind_c].append(stacks[stack])
lists_of_list = [x[::-1] for x in lists_of_list]
#coming to movement portion
move_list=my_list[1]
a=move_list.splitlines()
for array in a:
    temp = []
    b=array.split(' ')
    numbers,fr,to=int(b[1]), int(b[3]), int(b[5])
    for i in range(numbers):
        # appending the last element of the list at index fr-1 of lists_of_list to temp
        temp.append((lists_of_list[fr - 1].pop()))
    # reversing list
    temp.reverse()
    # It is iterating over temp and for each element j,
    # it is appending it to the list at index to-1 of lists_of_list
    for j in temp:
        lists_of_list[to - 1].append(j)

str = ""
for lists in lists_of_list:
    str += lists[-1]
print(str)

