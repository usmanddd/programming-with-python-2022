with open("day01.txt", "rt") as myfile:
    my_list = myfile.read()
    my_list=my_list.splitlines()
new_list=[]
elf=[]
# for each i in my_list, if it is not empty string, the integers are appended to elf list
# when we have empty strings available, then we append the elf list to new list 
# and make elf empty list
for i in my_list:
    if i!='':
        elf.append(int(i))
    else:
        new_list.append(elf)
        elf=[]
#sum of sub-calories
new_list_1 = [sum(l) for l in new_list]
#Answer-1
print(max(new_list_1))
#sort the list in descending order
largest_three_ = sorted(new_list_1, reverse=True)
largest_three = largest_three_[0] + largest_three_[1] + largest_three_[2]
#Answer-2
print(largest_three)