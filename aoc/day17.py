from itertools import cycle
with open('day17.txt', 'r') as f:
    my_input=f.read().strip()
shapes = [
    [(0,0),(1,0),(2,0),(3,0)], # -
    [(0,1),(1,1),(2,1),(1,0),(1,2)], # +
    [(0,0),(1,0),(2,0),(2,1),(2,2)], # reverse of L
    [(0,0),(0,1),(0,2),(0,3)], # |
    [(0,0),(0,1),(1,0),(1,1)] # square   
  ] 

# occupancy_map is used to check whether new shape is overlapping with already placed shapes
occupancy_map = set([(i, 0) for i in range(7)])
# cycle through shapes
cycle_shapes = cycle(shapes)
# cycle through input
cycle_input = cycle(my_input)
input_index = 0
shape_index = 0

height = 0
count = 0
cyclic_height_added = 0
max_count = 2022

cache = dict()
while count < max_count:
    original_shape = next(cycle_shapes)
    shape = []
    for coords in original_shape:
        # adding 2 to x-coordinates of current rock shape
        shape.append(coords[0] + 2)
        # adding 4 and height to y_coordinates of current rock shape
        shape.append(coords[1] + 4 + height)
    done = False
    shape_index += 1
    shape_index %= len(shapes)
    while not done:
        flag = False
        # check if input has <, then assign length = -1, shape of rock has to move left
        if next(cycle_input) == '<':
            dx = -1
        else:
        # else rock shape has to move right 
            dx = 1
        # modulo operator to keep in bounds
        input_index += 1
        input_index %= len(my_input)

        # then update the x-coordinate in shape because even are x and odd are y coordinates
        moved_shape = [value + dx if i % 2 == 0 else value for i, value in enumerate(shape)] 
        # check if it touches the bounds if list contains elements -1 or 7 in x-coordinates, then assign flag = 1
        if set(moved_shape[::2]).intersection([-1, 7]):
            flag = True
        # get pair of first and second elements of shape, then match pair to occupancy_map,
        # if it exist, set flag to 1 so to move back to previous position

        for pair in zip(moved_shape[0::2], moved_shape[1::2]):
            if pair in occupancy_map:
                flag = True
        # if flag is true it means, it means shape of rocks has gone out of bounds
        if flag is False:
            shape = moved_shape

        flag = False
        # update y-coordinates in odd positions by substracting 1 from y_values,
        # it moves shape of rocks down by 1 
        moved_shape = [value - 1 if i % 2 != 0 else value for i, value in enumerate(shape)] 
        # get pair of first and second elements of shape, then match pair to occupancy_map,
        # if it exist, set flag to 1
        for pair in zip(moved_shape[0::2], moved_shape[1::2]):
            if pair in occupancy_map:
                flag = True
        # if flag is true it means, it means shape of rocks has gone out of bounds
        if flag:
            done = True
        else:
            shape = moved_shape
    for index, item in enumerate(shape):
        # if current element is x_coordinate, then append occupancy_map with that element and also next element
        if index % 2 == 0:
            occupancy_map.add((shape[index], shape[index + 1]))
        # if current element is y_coordinate, then get maximum of height and that element
        if index % 2 != 0:
            height = max (height, item)
    # frozenset used to make it hashable
    # x-coor remains same and add 40 to y-coor and subtract height
    top20 = frozenset([(pair[0], pair[1] - height + 40) for pair in occupancy_map if pair[1] >= height - 40])
    identifier = (shape_index, input_index, top20)
    # skip forward as many times as possible based on the cyclic information found
    if identifier in cache:
        # if found in cache, rock shapes and the input are cycling
        old_height, old_count = cache[identifier]
        repetitions = (max_count - count) // (count - old_count)
        cyclic_height_added += (height - old_height) * repetitions
        count += (count - old_count) * repetitions

    cache[identifier] = height, count

    count += 1
# Answer-1 
print(height + cyclic_height_added)

##### Part-02 #####
with open('day17.txt', 'r') as f:
    my_input=f.read().strip()
shapes = [
    [(0,0),(1,0),(2,0),(3,0)], # -
    [(0,1),(1,1),(2,1),(1,0),(1,2)], # +
    [(0,0),(1,0),(2,0),(2,1),(2,2)], # reverse of L
    [(0,0),(0,1),(0,2),(0,3)], # |
    [(0,0),(0,1),(1,0),(1,1)] # square   
  ] 

# occupancy_map is used to check whether new shape is overlapping with already placed shapes
occupancy_map = set([(i, 0) for i in range(7)])
# cycle through shapes
cycle_shapes = cycle(shapes)
# cycle through input
cycle_input = cycle(my_input)
input_index = 0
shape_index = 0

height = 0
count = 0
cyclic_height_added = 0
max_count = 1000000000000

cache = dict()
while count < max_count:
    original_shape = next(cycle_shapes)
    shape = []
    for coords in original_shape:
        # adding 2 to x-coordinates of current rock shape
        shape.append(coords[0] + 2)
        # adding 4 and height to y_coordinates of current rock shape
        shape.append(coords[1] + 4 + height)
    done = False
    shape_index += 1
    shape_index %= len(shapes)
    while not done:
        flag = False
        # check if input has <, then assign length = -1, shape of rock has to move left
        if next(cycle_input) == '<':
            dx = -1
        else:
        # else rock shape has to move right 
            dx = 1
        # modulo operator to keep in bounds
        input_index += 1
        input_index %= len(my_input)

        # then update the x-coordinate in shape because even are x and odd are y coordinates
        moved_shape = [value + dx if i % 2 == 0 else value for i, value in enumerate(shape)] 
        # check if it touches the bounds if list contains elements -1 or 7 in x-coordinates, then assign flag = 1
        if set(moved_shape[::2]).intersection([-1, 7]):
            flag = True
        # get pair of first and second elements of shape, then match pair to occupancy_map,
        # if it exist, set flag to 1 so to move back to previous position

        for pair in zip(moved_shape[0::2], moved_shape[1::2]):
            if pair in occupancy_map:
                flag = True
        # if flag is true it means, it means shape of rocks has gone out of bounds
        if flag is False:
            shape = moved_shape

        flag = False
        # update y-coordinates in odd positions by substracting 1 from y_values,
        # it moves shape of rocks down by 1 
        moved_shape = [value - 1 if i % 2 != 0 else value for i, value in enumerate(shape)] 
        # get pair of first and second elements of shape, then match pair to occupancy_map,
        # if it exist, set flag to 1
        for pair in zip(moved_shape[0::2], moved_shape[1::2]):
            if pair in occupancy_map:
                flag = True
        # if flag is true it means, it means shape of rocks has gone out of bounds
        if flag:
            done = True
        else:
            shape = moved_shape
    for index, item in enumerate(shape):
        # if current element is x_coordinate, then append occupancy_map with that element and also next element
        if index % 2 == 0:
            occupancy_map.add((shape[index], shape[index + 1]))
        # if current element is y_coordinate, then get maximum of height and that element
        if index % 2 != 0:
            height = max (height, item)
    # frozenset used to make it hashable
    # x-coor remains same and add 40 to y-coor and subtract height
    top20 = frozenset([(pair[0], pair[1] - height + 40) for pair in occupancy_map if pair[1] >= height - 40])
    identifier = (shape_index, input_index, top20)
    # skip forward as many times as possible based on the cyclic information found
    if identifier in cache:
        # if found in cache, rock shapes and the input are cycling
        old_height, old_count = cache[identifier]
        repetitions = (max_count - count) // (count - old_count)
        cyclic_height_added += (height - old_height) * repetitions
        count += (count - old_count) * repetitions

    cache[identifier] = height, count

    count += 1
# Answer-2 
print(height + cyclic_height_added)


##### Reference #####
# (https://www.reddit.com/r/adventofcode/comments/znykq2/2022_day_17_solutions/)
