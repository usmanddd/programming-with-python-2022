with open('day21.txt') as f:
    my_input=f.read().strip().splitlines()

list_monkeys=[]

while my_input:
    # take one by one string from input
    take_one_=my_input.pop(0)
    # perform splitting by : and space
    a,b=take_one_.split(': ')
    # if second element is integer, then store its name in list and that int, finally sort it
    if b.isnumeric():
        list_monkeys.append((a,int(b)))
    else:
    # else get three elements, between two is operation to carry
        job_1, process, job_2=b.split()
        job_1_value=None
        job_2_value=None
        for monkey in list_monkeys:
            # if first element of tuple in list matches with job_1
            # assign its value to job_1_value  
            if monkey[0]==job_1:
                job_1_value=monkey[1]
            # elif first element of tuple in list matches with job_2
            # assign its value to job_2_value
            elif monkey[0]==job_2:
                job_2_value=monkey[1]
            # if both have not NONE values
        if job_1_value is not None and job_2_value is not None:
            # perform the operations and then append the names as well as values
            if process=='+':
                add = job_1_value + job_2_value
                list_monkeys.append((a , add))
            elif process=='-':
                minus = job_1_value - job_2_value
                list_monkeys.append((a , minus))
            elif process=='*':
                multiply = job_1_value * job_2_value
                list_monkeys.append((a , multiply))
            elif process=='/':
                divide = job_1_value / job_2_value
                list_monkeys.append((a , divide))
        else:
            my_input.append(take_one_)

# find value of root monkey
value_root=0
for monkey in list_monkeys:
    if monkey[0]=='root':
        value_root=monkey[1]
# Answer-1
print(value_root)



##### Reference #####
# (https://www.reddit.com/r/adventofcode/comments/zrav4h/2022_day_21_solutions/)


    