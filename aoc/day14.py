with open('day14.txt') as f:
    lines=f.read().strip().split('\n')
paths_of_rock=set()
for event in lines:
  events = []
  #splitting on the basis of -> first
  for i in event.split(" -> "):
    #split and map to x and y
    x, y = map(int, i.split(","))
    #append tuples to list
    events.append((x, y))
    #adding all the values of x and y coordinates to paths of rock
    i = 0
    while i < len(events) - 1:
      e_1 = events[i]
      e_2 = events[i + 1]
      #minimum values
      x1, y1 = sorted((e_1[0], e_2[0]))[0], sorted((e_1[1], e_2[1]))[0]
      #maximum values
      x2, y2 = sorted((e_1[0], e_2[0]))[1], sorted((e_1[1], e_2[1]))[1]
      # adding all x and y coordinates to set
      for x_coor in range(x1, x2 + 1):
          for y_coor in range(y1, y2 + 1):
            paths_of_rock.add((x_coor, y_coor))
      i += 1
# Find the maximum value of y in paths_of_rock
max_y_coordinate = 0
for rock in paths_of_rock:
    i, y_coor = rock
    if y_coor > max_y_coordinate:
        max_y_coordinate = y_coor
#loop will vanish if (x_coor, y_coor) approaches to (500, 0) or hit by rocks
count=0 # number of steps taken
e_1=0
while (x_coor, y_coor) != (500, 0):
    if (x_coor, y_coor) in paths_of_rock:  
      (x_coor, y_coor) = (500, 0)
    #if selected y_coor is greater than maximum value, and not e_1, then e_1 updated to count
    if (y_coor > max_y_coordinate) and (not e_1) : 
      e_1 = count
    # downward, left-down, right-down
    directions = [(0, 1), (-1, 1), (1, 1)]
    for dx, dy in directions:
      x_coor_new = x_coor + dx
      y_coor_new = y_coor + dy
      #update values of coordinates
      if (x_coor_new, y_coor_new) not in paths_of_rock and y_coor_new <= max_y_coordinate+1:
        x_coor, y_coor = x_coor_new, y_coor_new
        break
    else:
      #count +1 if the end point reached 
        count+=1
        paths_of_rock.add((x_coor, y_coor))
#Answer-1:
print(e_1)

###Part-2###
#Answer-2:
print(count)


##### Reference #####
# (https://www.reddit.com/r/adventofcode/comments/zli1rd/2022_day_14_solutions/)

