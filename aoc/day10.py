with open("day10.txt") as f:
    lines = f.read().strip().splitlines()
#declare list having 1 to make iterations
sum_=[1]
for i in lines:
    b=i.split(' ')
    #if it is noop on index 0, iterate by 1 time
    if b[0]=='noop':
       sum_.extend([sum_[-1]])
    if b[0]=='addx':
        # if it is addx on index 0, iterate by 1 time and 
        # also iterate with sum of integer on index 1 and last value in sum_
        sum_.extend([sum_[-1]])
        addit=int(b[1]) +sum_[-1]
        sum_.extend([addit])
list_of_cycles=[20,60,100,140,180,220]
new_list=[]
#for all cycles given, find signal-strength
for i in list_of_cycles:
    relative_sum=i*sum_[i-1]
    new_list.append(relative_sum)
#Answer-1
print(sum_)
print(sum(new_list))
###Part-2###
k = 0
while k < len(sum_):
    number = sum_[k]
    #  drawing # if divisible by 40
    # # represent value close to iteration and space if value is far away
    # If the remainder of dividing "k" by 40 is 0,
    # it means that "k" is divisible by 40, and the code prints a newline character ("\n")
    if divmod(k, 40)[1] == 0:
        print("\n", end='')
    # difference between the current value in the "sum_" list and the current iteration "k"
    d = number - divmod(k,40)[1]
    print('#' if abs(d) <= 1 else '.', end='')
    k += 1


#####Reference#####
# (https://www.reddit.com/r/adventofcode/comments/zhjfo4/2022_day_10_solutions/)
