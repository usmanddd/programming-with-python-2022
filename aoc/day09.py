with open("day09.txt") as f:
    lines= f.read().strip().splitlines()
#print(lines)
h=(0,0)
t=(0,0)
moves={(t)}
#dictionary to make directions possible
directions = {
    'R': lambda x: (x[0]+1, x[1]),
    'L': lambda x: (x[0]-1, x[1]),
    'U': lambda x: (x[0], x[1]+1),
    'D': lambda x: (x[0], x[1]-1),
}
#apply directions to heads and consequently tail will be after head
for i in lines:
    a, b = i.split()
    for i2 in range(int(b)):
        last_h = h
        # map the direction to the current position of head
        h = directions[a](h)
        # if distance between head and tail > 1 
        if abs(h[0]-t[0])>1 or abs(h[1]-t[1]) >1:
            # update tail to the last position of head
            t=last_h
            # add to moves 
            moves.add(t)
print(len(moves))
###Part-2###
nodes = []
a=10
for i in range(a):
    nodes.append((0, 0))
moves={(nodes[-1])}
for i in lines:
    a,b=i.split()
    for i2 in range(int(b)):
        # moves in x and y coordinates
        # If the value of "a" is "R", the x-coordinate of the point is increased by 1
        if a=='R':
            nodes[0] = (nodes[0][0]+1, nodes[0][1])
        # If the value of "a" is "D", the y-coordinate of the point is decreased by 1
        if a== 'D':
            nodes[0] = (nodes[0][0], nodes[0][1] - 1)
        # If the value of "a" is "U", the y-coordinate of the point is increased by 1
        if a== 'U':
            nodes[0] = (nodes[0][0] , nodes[0][1]+1)
        # If the value of "a" is "L", the x-coordinate of the point is decreased by 1
        if a== 'L':
            nodes[0] = (nodes[0][0] - 1, nodes[0][1])
        # compares the positions of the first element in "nodes" with the rest of the elements,
        # and if the difference between them is greater than 1 in either the x or y coordinate, 
        # it updates the position of the current element to be adjacent to the previous one 
        # and adds that position to the "moves" set.
        for comp in range(9):
            if abs(nodes[comp][0] - nodes[comp+1][0]) > 1 or abs(nodes[comp][1] - nodes[comp+1][1]) > 1:
                if (nodes[comp][0]-nodes[comp+1][0])>0 and (nodes[comp][1] - nodes[comp+1][1])==0:
                    nodes[comp+1]=(nodes[comp+1][0]+1,nodes[comp+1][1])
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])<0 and (nodes[comp][1] - nodes[comp+1][1])==0:
                    nodes[comp+1]=(nodes[comp+1][0]-1,nodes[comp+1][1])
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])==0 and (nodes[comp][1] - nodes[comp+1][1])==0:
                    nodes[comp+1]=(nodes[comp+1][0],nodes[comp+1][1])
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])==0 and (nodes[comp][1] - nodes[comp+1][1])>0:
                    nodes[comp+1]=(nodes[comp+1][0],nodes[comp+1][1]+1)
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])==0 and (nodes[comp][1] - nodes[comp+1][1])<0:
                    nodes[comp+1]=(nodes[comp+1][0],nodes[comp+1][1]-1)
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])>0 and (nodes[comp][1] - nodes[comp+1][1])>0:
                    nodes[comp+1]=(nodes[comp+1][0]+1,nodes[comp+1][1]+1)
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])>0 and (nodes[comp][1] - nodes[comp+1][1])<0:
                    nodes[comp+1]=(nodes[comp+1][0]+1,nodes[comp+1][1]-1)
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])<0 and (nodes[comp][1] - nodes[comp+1][1])>0:
                    nodes[comp+1]=(nodes[comp+1][0]-1,nodes[comp+1][1]+1)
                    moves.add(nodes[-1])
                if (nodes[comp][0]-nodes[comp+1][0])<0 and (nodes[comp][1] - nodes[comp+1][1])<0:
                    nodes[comp+1]=(nodes[comp+1][0]-1,nodes[comp+1][1]-1)
                    moves.add(nodes[-1])         
# Answer-2
print(len(moves))


#####Reference#####
#(https://www.reddit.com/r/adventofcode/comments/zgnice/2022_day_9_solutions/)
