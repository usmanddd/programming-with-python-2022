with open("day02.txt", "rt") as myfile:
    my_list = myfile.read()
    my_list=my_list.splitlines()
new_list=my_list.copy()
# Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock.
# If both players choose the same shape, the round instead ends in a draw.
# A = rock for opponent, B = paper for opponent, C = Scissor for opponent
# X = rock for me, Y = paper for me, Z = scissor for me
for i in range(len(my_list)):
    if my_list[i] == 'A Y':  # I won
       my_list[i] = 8        # 2 (paper) + 6 (won) 
    if my_list[i] == 'B X' : # I lost
       my_list[i] = 1        # 1 (rock) + 0 (lost)
    if my_list[i] == 'C Z' : # draw
       my_list[i] = 6        # 3 (scissor) + 3 (draw) 
    if my_list[i] == 'A Z' : # I lost
       my_list[i] = 3        # 3 (scissor) + 0 (lose)
    if my_list[i] == 'A X' : # draw
       my_list[i] = 4        # 1 (rock) + 3 (draw)
    if my_list[i] == 'B Y' : # draw
       my_list[i] = 5        # 2 (paper) + 3 (draw)
    if my_list[i] == 'B Z' : # I won
       my_list[i] = 9        # 3 (scissor) + 6 (won)
    if my_list[i] == 'C X' : # I won
       my_list[i] = 7        # 1 (rock) + 6 (won)
    if my_list[i] == 'C Y' : # I lost
       my_list[i] = 2        # 2 (paper) + 0 (lost)
#Answer-1
print(sum(my_list))
my_list=new_list.copy()
#  X means you need to lose, Y means you need to end the round in a draw, 
# and Z means you need to win
for i in range(len(my_list)):
    if my_list[i] == 'A X' : # need to select scissors  
       my_list[i] = 3
    if my_list[i] == 'A Y': # need to select rock
       my_list[i] = 4
    if my_list[i] == 'A Z' : # need to paper
       my_list[i] = 8
    if my_list[i] == 'B X' : # need to select rock
       my_list[i] = 1
    if my_list[i] == 'B Y' : # need to select paper
       my_list[i] = 5
    if my_list[i] == 'B Z' : # need to select scissors
       my_list[i] = 9
    if my_list[i] == 'C X' : # need to select paper
       my_list[i] = 2
    if my_list[i] == 'C Y' : # need to select scissors
       my_list[i] = 6
    if my_list[i] == 'C Z' : # need to select rock
       my_list[i] = 7
#Answer-2
print(sum(my_list))