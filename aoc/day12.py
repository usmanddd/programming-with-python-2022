with open('day12.txt') as f:
    my_input=f.read().strip().splitlines()
# check if S and E exist in 2D list,
# assign the coordinates for start and end positions in dictionary
positions = {}
for row in range(len(my_input)):
    for col in range(len(my_input[row])):
        if my_input[row][col] == "S":
            positions["start"] = (row, col)
        elif my_input[row][col] == "E":
            positions["end"] = (row, col)
#  2D list, replacing all occurrences of "S" and "E" with "." 
# Returning a new 2D list with these changes

updated_list = []
for row in my_input:
    new_row = "".join(row)
    new_row = new_row.replace("S", ".").replace("E", ".")
    new_row = list(new_row)
    updated_list.append(new_row)
# assign values row and column values to the start and end keys 
start_row, start_column = positions["start"]
end_row, end_column = positions["end"]
# replacing the character at start row and start column with a
updated_list[start_row][start_column] = "a"
# replacing the character at end row and end column with z
updated_list[end_row][end_column] = "z"
# 2D list of False with same dimension of updated list to mark each visited location
visited = [[False for _ in range(len(updated_list[0]))] for _ in range(len(updated_list))]


def bfs(mesh, start_row, start_column, end_row, end_column, visited):
    # initialize queue at starting point
    queue = [(start_row, start_column)]
    # mark positions visited
    visited[start_row][start_column] = True

    while queue:
        # take row and column of each point
        row, col = queue.pop(0)
        # check if it is end point
        if row == end_row and col == end_column:
        # if it is end point then mark as visited by returning number of steps needed to reach end point
            return visited[row][col] - 1
        # if it is not end point, then function iterates over 4 possible directions, up, down, left, and right
        shifts = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        for shift in shifts:
            new_row = row + shift[0]
            new_col = col + shift[1]
            # if new coordinates are inside boundaries of 2D list and not in visited list
            if 0 <= new_row < len(mesh) and 0 <= new_col < len(mesh[0]) and not visited[new_row][new_col]:
                # then check it is exact neighbour
                if mesh[new_row][new_col].encode()[0]- mesh[row][col].encode()[0] -1 <= 0:
                    # also updating number of steps taken to reach new point
                    visited[new_row][new_col] = visited[row][col] + 1
                    # also appending list to iterate further
                    queue.append((new_row, new_col))
    return -1
# Answer-1
print(bfs(updated_list, start_row, start_column, end_row, end_column, visited))

####Part-02####
with open('day12.txt') as f:
    my_input=f.read().strip().splitlines()

positions = {}
# check if S and E exist in 2D list,
# assign the coordinates for start and end positions in dictionary
for row in range(len(my_input)):
    for col in range(len(my_input[row])):
        if my_input[row][col] == "S":
            positions["start"] = (row, col)
        elif my_input[row][col] == "E":
            positions["end"] = (row, col)
#  2D list, replacing all occurrences of "S" and "E" with "." 
# Returning a new 2D list with these changes
updated_list = []
for row in my_input:
    new_row = "".join(row)
    new_row = new_row.replace("S", ".").replace("E", ".")
    new_row = list(new_row)
    updated_list.append(new_row)
# assign values row and column values to the start and end keys
start_row, start_column = positions["start"]
end_row, end_column = positions["end"]
# replacing the character at start row and start column with a
updated_list[start_row][start_column] = "a"
# replacing the character at end row and end column with z
updated_list[end_row][end_column] = "z"

# 2D list of False with same dimension of updated list to mark each visited location
visited = [[False for _ in range(len(updated_list[0]))] for _ in range(len(updated_list))]


def find_shortest_path(updated_list, end_row, end_column, visited):
    stack = [(0, end_row, end_column)]
    min_path_length = float('inf')
    found_a = False
    while stack:
        path, row, col = stack.pop(0)
        # shifting rows and columns
        shifts = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        for shift in shifts:
            new_row = row + shift[0]
            new_col = col + shift[1]   
            # if the new indices are within the bounds of the updated_list
            # and if the current position has been visited before         
            if not (0 <= new_row < len(updated_list) and 0 <= new_col < len(updated_list[0])) or visited[new_row][new_col]:
                continue
            # new position is the next alphabetical character after the current position
            if updated_list[new_row][new_col].encode()[0] -updated_list[row][col].encode()[0] + 1 < 0 :
                continue
            # If the updated_list at the new indices is equal to "a"
            if updated_list[new_row][new_col] == "a":
                if not found_a:
                    # minimum path length
                    min_path_length = path + 1
                    found_a = True
            visited[new_row][new_col] = visited[row][col] + 1
            stack.append((path + 1, new_row, new_col))
    print(min_path_length)
# Answer-2
find_shortest_path(updated_list, end_row, end_column, visited)

##### Reference #####
# (https://favtutor.com/blogs/breadth-first-search-python)
# (https://www.educative.io/answers/how-to-implement-a-breadth-first-search-in-python)
# (https://www.reddit.com/r/adventofcode/comments/zjnruc/2022_day_12_solutions/)