from functools import cmp_to_key
import ast
with open('day13.txt', "r") as f:
    list_1 = f.read().strip()
    lists=list_1.split("\n\n")
def test(a1, a2):
    #check if both are integers, then apply left must be less than right
    if type(a1) is int and type(a2) is int:
        return (a1>a2)-(a2>a1)
    #if integer convert to lists
    if type(a1) is int:
        a1 = [a1]
    if type(a2) is int:
        a2 = [a2]
    #apply above conditions on all indexes of both lists
    i = 0
    while i < len(a1) and i < len(a2):
        res = test(a1[i], a2[i])
        if res != 0:
            return res
        i += 1
    #check lengths
    if i < len(a1):
        return 1
    if i < len(a2):
        return -1
    return 0
count = 0
index = 0
while index < len(lists):
    both_lists = lists[index]
    #print(both_lists)
    l1, l2 = map(ast.literal_eval, both_lists.split("\n"))
    if test(l1, l2) == -1:
        # as we need to find indices
        count+= index
        count+= 1
    index += 1
#Answer-1
print(count)

###Part-2###
#parsing (splitting) on the basis of single line by ast
new_list = []
for i in list_1.split("\n"):
    if i:
        new_list.append(ast.literal_eval(i))
#adding [[2]] and [[6]]
new_list.append([[2]])
new_list.append([[6]])
# sort the list using test function created above
sorted_list=sorted(new_list, key=cmp_to_key(test))
#find indices of [[2]] and [[6]] in sorted list, then multiply them
count_1 = 1
index = 0
while index < len(sorted_list):
    if sorted_list[index] in [[[2]], [[6]]]:
        count_1 *= index + 1
    index += 1
#Answer-2
print(count_1)

##### Reference #####
# (https://www.reddit.com/r/adventofcode/comments/zkmyh4/2022_day_13_solutions)



