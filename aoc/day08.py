with open("day08.txt") as f:
    lines = f.read().strip().split('\n')
n_visible = 0
#iteration of rows from index_1 to index_n-1 
for i in range(1, len(lines) - 1):
    #iteration of columns from index_1 to index_n-1 
    for j in range(1, len(lines[0]) - 1):
        # for above
        visible_above = True
        for i2 in range(i):
            if int(lines[i2][j]) >= int(lines[i][j]):
                visible_above = False
                break
        # for below
        visible_below = True
        for i2 in range(i+1, len(lines)):
            if int(lines[i2][j]) >= int(lines[i][j]):
                visible_below = False
                break
        #for left
        visible_left = True
        for j2 in range(j):
            if int(lines[i][j2]) >= int(lines[i][j]):
                visible_left = False
                break
        #for right
        visible_right = True
        for j2 in range(j+1, len(lines[0])):
            if int(lines[i][j2]) >= int(lines[i][j]):
                visible_right = False
                break
       #increment of n_visible by 1 if any of above conditions true
        if visible_above or visible_below or visible_left or visible_right:
            n_visible += 1
#considering the outer boundaries as they are visible
n_visible += 2 * len(lines)
n_visible += 2 * (len(lines[0]) - 2)
#Answer-1
print(n_visible)

###Part-2###

n_visible_1=0
#iteration of rows
for i in range(len(lines)):
    #iteration of columns
    for j in range(len(lines[0])):
        count=1
        
        # check visibility in four directions
        #Then multiply all the count of all four direction and 
        # take the max from the all the elements which gives max visibility count
        
        #above
        count_1=0
        for i2 in range(i-1,-1,-1):
            if int(lines[i2][j])<int(lines[i][j]):
                count_1+=1
            else:
                count_1+=1
                break
        count*=count_1
        #below
        count_2=0
        for i2 in range(i+1,len(lines)):
            if int(lines[i2][j])<int(lines[i][j]):
                count_2+=1
            else:
                count_2+=1
                break
        count*=count_2
        #left
        count_3=0
        for j2 in range(j-1,-1,-1):
            if int(lines[i][j2])<int(lines[i][j]):
                count_3+=1
            else:
                count_3+=1
                break
        count*=count_3
        #right
        count_4=0
        for j2 in range(j+1,len(lines[0])):
            if int(lines[i][j2])<int(lines[i][j]):
                count_4+=1
            else:
                count_4+=1
                break
        count*=count_4
        n_visible_1=max(count,n_visible_1)
#Answer-2
print(n_visible_1)



#####Reference#####
#(https://www.reddit.com/r/adventofcode/comments/zfpnka/2022_day_8_solutions/)