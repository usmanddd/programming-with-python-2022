with open('day11.txt') as f:
    lines=f.read().strip()
#split by double lines of space between two clusters of monkey's data
lines = lines.split("\n\n")
all_monk = []
for i in lines:
  #split each by difference of new line
  lines = i.split('\n')
  single_monkey_split = []
  #separate starting items
  parts = lines[1].split(": ")
# check if there are two parts
  if len(parts) == 2:
    # split the second part on ", "
    items = parts[1].split(", ")
  else:
    items = []
  #map items to integers
  single_monkey_split += [list(map(int, items))]
        # split operations on basis of "="
  parts = lines[2].split("=")

# check if there are two parts
  if len(parts) == 2:
    item = parts[1]
  else:
    item = None
  #making lambda functions of every operation
  operations = [eval("lambda old:" + item)]
  single_monkey_split += operations
  #get numbers which needs to divide, also throw numbers for true and false cases in single list
  numbers_3 = list(int(line.split()[-1]) for line in lines[3:])
  single_monkey_split += numbers_3
  #add all lists in single list
  all_monk.extend([single_monkey_split])
#made empty list for all monkeys 
count = [0 for _ in all_monk]
i = 0
while i < 20:
  for i2, data_one_monkey_ in enumerate(all_monk):
    values, func, divisor, group1, group2=data_one_monkey_
    #change values by performing function divisible by 3
    transformed_values = [func(x) // 3 for x in values]
    #clear all original values in our list
    values.clear()
    #extend group1 if each item in transformed has modulus 0
    all_monk[group1][0].extend(item for item in transformed_values if item % divisor == 0)
    #extend group2 if each item in transformed has modulus not equals 0
    all_monk[group2][0].extend(item for item in transformed_values if item % divisor != 0)
    #update count with adding length of transformed values
    count[i2] = count[i2] + len(transformed_values)
  i += 1

largest = max(count)
count.remove(largest)
second_largest = max(count)
monkey_business=largest*second_largest
#Answer-1
print(monkey_business)


#####Part-02####
with open('day11.txt') as f:
    lines=f.read().strip()
#split by double lines of space between two clusters of monkey's data
lines = lines.split("\n\n")
all_monk = []
for i in lines:
  #split each by difference of new line
  lines = i.split('\n')
  single_monkey_split = []
  #separate starting items
  parts = lines[1].split(": ")
# check if there are two parts
  if len(parts) == 2:
    # split the second part on ", "
    items = parts[1].split(", ")
  else:
    items = []
  #map items to integers
  single_monkey_split += [list(map(int, items))]
        # split on "="
  parts = lines[2].split("=")

# check if there are two parts
  if len(parts) == 2:
    item = parts[1]
  else:
    item = None
  #making lambda functions of every operation
  operations=[eval("lambda old:" + item)]
  single_monkey_split += operations
  #get numbers which needs to divide, also throw numbers for true and false cases in single list
  numbers_3=list(int(line.split()[-1]) for line in lines[3:])
  single_monkey_split += numbers_3
  #add all lists in single list
  all_monk.extend([single_monkey_split])
#made empty list for all monkeys 
count_1 = [0 for _ in all_monk]
#multiply the divisor elements with each other 
multiply_divisors = 1
for i in (i[2] for i in all_monk):
    multiply_divisors *= i
#now increase the loop upto 10000
count = 0
while count < 10000:
  for i, data_one_monkey in enumerate(all_monk):
    values_1, func, divisor_1, group_1, group_2 = data_one_monkey
    #change items in values by performing function divisible by multiply_divisors and check if their modulus==0
    transformed_items_1 = [func(item) % multiply_divisors for item in values_1 if func(item) % divisor_1 == 0]
    transformed_items_2 = [func(item) % multiply_divisors for item in values_1 if func(item) % divisor_1 != 0]
    #extend group-1 if each item in transformed has modulus 0
    all_monk[group_1][0].extend(transformed_items_1)
    #extend group2 if each item in transformed has modulus not equals 0
    all_monk[group_2][0].extend(transformed_items_2)
        #update count with adding length of values_1
    count_1[i] = count_1[i] + len(values_1)
    values_1.clear()
  count += 1

largest_1 = max(count_1)
count_1.remove(largest_1)
second_largest_1 = max(count_1)
#Answer-1
monkey_business_1=largest_1*second_largest_1
print(monkey_business_1)


#####Reference#####
#(https://www.reddit.com/r/adventofcode/comments/zifqmh/2022_day_11_solutions/)