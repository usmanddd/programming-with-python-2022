from collections import Counter
counter = Counter()
with open("day07.txt", "rt") as myfile:
    my_list = myfile.read().strip()
    my_list=my_list.splitlines()
#filter if first index after splitting not equal to 'dir' and second is not 'ls'
a_values = [i.strip().split() for i in my_list]
filtered_a_values = filter(lambda a: a[0] != 'dir' and a[1] != 'ls', a_values)
#apply conditions on filtered values
new_list=[]
for a in filtered_a_values:
    if a[1] == 'cd':
        if a[2] == '..':
            # removes last element
            new_list.pop()
        else:
            # add last element
            new_list.extend([a[2]])
    else:
        b = int(a[0])
        for i in range(1, len(new_list)+1):
            # directory_name is created by joining the elements of new_list
            # from index 0 to the current iteration value using the join method with the separator '/'
            directory_name = '/'.join(new_list[:i])
            # The value in the counter object at the key directory_name is incremented by the value of b
            counter[directory_name] += b
            

#Compute the total sum of the values in the counter object
total_sum = sum(y for x, y in counter.items() if y <= 100000)
#Answer-1
print(total_sum)
#using condition for max used
max_used= 70000000 - 30000000
full_usage = counter['/']
delete = full_usage -max_used
#Compute minimum of all value of y, which are >=delete
min_value = min([y for x, y in counter.items() if y >= delete])
#Answer-2
print(min_value)


#####Reference#####
#(https://www.reddit.com/r/adventofcode/comments/zesk40/2022_day_7_solutions/)