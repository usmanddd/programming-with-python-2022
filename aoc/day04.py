with open("day04.txt", "rt") as myfile:
    my_list = myfile.read()
    my_list=my_list.split()
# splitting by ,
d = []
for s in my_list:
    d.append(s.split(','))
# internal splitting by-
result = []
for l in d:
    inner_result = []
    for s in l:
        inner_result.append(s.split('-'))
    result.append(inner_result)
# comparison
# example: a=[6,9], b= [7,8] 2nd is contained in 1st if a[0] <= b[0] and a[1] >= b[1]
# example: a= [6,7] b= [5,8] 1st is contained in 2nd if b[0] <= a[0] and b[1] >= a[1]
count_1=0
for pairs in result:
    x=(int(pairs[0][0]),int(pairs[0][1]))
    y=(int(pairs[1][0]),int(pairs[1][1]))
    match(x,y):
        case((a,b),(c,d)) if a <= c and b >= d:
            count_1+=1
        case((a,b),(c,d)) if c <= a and d >= b:
            count_1+=1

#Answer-1
print(count_1)
#####Part-2######
# example: a=[6,9], b= [8,10] overlap occurs if a[1] >= b[0] and a[0] <= b[1]
count_2=0
for pairs in result:
    x = (int(pairs[0][0]), int(pairs[0][1]))
    y = (int(pairs[1][0]), int(pairs[1][1]))
    match(x,y):
        case((a,b),(c,d)) if b >= c and d >= a:
            count_2 += 1
#Answer-2
print(count_2)