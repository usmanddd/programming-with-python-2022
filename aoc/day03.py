import string
with open("day03.txt", "rt") as myfile:
    my_list = myfile.read()
    my_list = my_list.splitlines()

#divide each string into halves
new_list = []
for term in range(len(my_list)):
    length = len(my_list[term])
    middle_index = length//2
    first_half = my_list[term][:middle_index]
    new_list.append(first_half)
    second_half = my_list[term][middle_index:]
    new_list.append(second_half)
# make lists of two chunks of strings
def divide_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]
n = 2
x = list(divide_chunks(new_list, n))
# finding common between sublists in list
w_list=[]
for i in x:
    d=set(i[0]).intersection(i[1])
    w_list.append(d)
#convert sets to list of lists
p_list=[]
for i in w_list:
    a = list(i)
    p_list.append(a)
# convert sublists to strings
flat_3 = []
for l in p_list:
    flat_3.extend(l)
# joining upper and lowers separately
uppers = [l for l in flat_3 if l.isupper()]
a=''.join(uppers)
lowers = [l for l in flat_3 if l.islower()]
b=''.join(lowers)
#convert alphabets to numbers
values = dict()
for index, letter in enumerate(string.ascii_lowercase):
   values[letter] = index +1
flat_4=[]
for i in b:
    f=values[i]
    flat_4.append(f)
out_1=sum(flat_4)
flat_5=[]
for i in a:
    k=ord(i)-38  # example: ord(A) - 38 = 65 - 38 = 27
    flat_5.append(k)
out_2=sum(flat_5)
# Answer-1
out=out_1+out_2
print(out)


#####Part-2#####
#divide each list into three chunks
def divide_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]
n=3
x = list(divide_chunks(my_list, n))
#finding common between sublists in list
w_list=[]
for i in x:
    d=set(i[0]).intersection(i[1]).intersection(i[2])
    w_list.append(d)
    i=+1
#convert sets to list of lists
p_list=[]
for i in w_list:
    a=list(i)
    p_list.append(a)
#convert sublists to strings
flat_3 = []
for l in p_list:
    flat_3.extend(l)
#joining upper and lowers separately
uppers = [l for l in flat_3 if l.isupper()]
a=''.join(uppers)
lowers = [l for l in flat_3 if l.islower()]
b=''.join(lowers)
#convert alphabets to numbers
flat_4=[]
for i in b:
    f=values[i]
    flat_4.append(f)
#print(flat_4)
out_1=sum(flat_4)
flat_5=[]
for i in a:
    k=ord(i)-38
    flat_5.append(k)
out_2=sum(flat_5)
# Answer-2
out=out_1+out_2
print(out)