with open("day06.txt", "rt") as myfile:
           my_list = myfile.read().strip()
def aoc6_1():
    if len(my_list) < 4:
        return -1
    for character in range(len(my_list)):
        # For each iteration, a new set is created that consists of a portion of my_list
        # starting from the current iteration index and including the next 4 elements
        a = set(my_list[character:character+4])
        #  if the length of the set a is equal to 4
        #  the function returns the current iteration index plus 4.
        if len(a) == 4:
            return character+4
    return -1

print(aoc6_1())

def aoc6_2():
    if len(my_list) < 14:
        return -1
    for character in range(len(my_list)):
        # For each iteration, a new set is created that consists of a portion of my_list
        # starting from the current iteration index and including the next 14 elements
        a = set(my_list[character:character+14])
        #  if the length of the set a is equal to 14
        #  the function returns the current iteration index plus 14
        if len(a) == 14:
            return character+14
    return -1

print(aoc6_2())