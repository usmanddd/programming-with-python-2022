with open('day20.txt') as file:
    input_ = file.read().strip().splitlines()
# make list of numbers as well as indexes using enumerate
number = []
indexes_num = []
for i,num in enumerate(input_):
    number.append(int(num))
    indexes_num.append(i)
# change values of indexes for each number
i = 0
while i < len(number):
    # find index of number
    index_seq = indexes_num.index(i)
    # remove number at that index
    indexes_num.remove(indexes_num[index_seq])
    # divide index of current number by length of indexes and take remainder
    _, remainder = divmod(index_seq + number[i], len(indexes_num))
    # insert remainder to indexes
    indexes_num = indexes_num[:remainder] + [i] + indexes_num[remainder:]
    i+=1


index_len=len(indexes_num)
# find occurance of zero at indexes
for i, elem in enumerate(indexes_num):
    if elem == number.index(0):
        matched = i
        break

total = 0
for num in range(1000, 3001, 1000):
    # find wrapped indexes for each value
    wrap_index = (matched + num) - index_len * ((matched + num) // index_len)
    # look for that index in list of numbers
    index = indexes_num[wrap_index]
    # find element of that index position
    value = number[index]
    # add all values
    total += value
# Answer-1
print(total)

### Rart-02 ###
with open('day20.txt') as file:
    input_ = file.read().strip().splitlines()

# make list of numbers as well as indexes using enumerate
number = []
indexes_num = []
for i,num in enumerate(input_):
    number.append(int(num))
    indexes_num.append(i)
    
# multiply all numbers by 
multiplied_list = [num * 811589153 for num in number]

# change values of indexes for each number
# iterate the loop for 10 iterations
i2=0
while i2 < 10:
    for i in range(len(multiplied_list)):
        # find index of number
        index_seq = indexes_num.index(i)
        # remove number at that index
        indexes_num.remove(indexes_num[index_seq])
        # divide index of current number by length of indexes and take remainder
        _, index = divmod(index_seq + multiplied_list[i], len(indexes_num))
        # insert remainder to indexes
        indexes_num = indexes_num[:index] + [i] + indexes_num[index:]
    i2+=1


index_len=len(indexes_num)
# find first occurance of zero at indexes
for i, elem in enumerate(indexes_num):
    if elem == multiplied_list.index(0):
        matched = i
        break

total = 0
for num in range(1000, 3001, 1000):
    # find wrapped indexes for each value
    wrapped_index = (matched + num) - index_len * ((matched + num) // index_len)
    # look for that index in list of numbers
    index = indexes_num[wrapped_index]
    # find element of that index position
    value = multiplied_list[index]
    # add all values
    total += value
# Answer-02
print(total)


##### Reference #####
# (https://www.reddit.com/r/adventofcode/comments/zqezkn/2022_day_20_solutions/)